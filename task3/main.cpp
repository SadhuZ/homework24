#include <iostream>
#include <cmath>

using namespace std;

struct vector
{
    double x;
    double y;
};

vector add(const vector& a, const vector& b){
    vector res = {a.x + b.x, a.y + b.y};
    return res;
}

vector subtract(const vector& a, const vector& b){
    vector res = {a.x - b.x, a.y - b.y};
    return res;
}

vector scale(const vector& a, const double b){
    vector res = {b * a.x, b * a.y};
    return res;
}

double length(const vector& a){
    double res = sqrt(a.x * a.x + a.y * a.y);
    return res;
}

vector normalize(const vector& a){
    double len = length(a);
    vector res = {a.x / len, a.y / len};
    return res;
}

vector GetVector(){
    vector vec;
    cout << "Enter vector coordinates:" << endl;
    cout << "x = ";
    cin >> vec.x;
    cout << "y = ";
    cin >> vec.y;
    return vec;
}

double GetScalar(){
    double scalar;
    cout << "Enter scalar: ";
    cin >> scalar;
    return scalar;
}

int main()
{
    string operation;
    vector resVector = {0, 0};
    double res = 0;
    while (operation != "quit") {
        cout << "Enter operation (add, subtract, scale, length, normalize, quit): ";
        cin >> operation;

        if(operation == "add" || operation == "subtract"){
            cout << "First vector. ";
            vector a = GetVector();
            cout << "Second vector. ";
            vector b = GetVector();
            resVector = (operation == "add") ? add(a, b) : subtract(a, b);
        }
        else if(operation == "scale"){

            resVector = scale(GetVector(), GetScalar());
        }
        else if(operation == "length"){
            res = length(GetVector());
        }
        else if(operation == "normalize"){
            resVector = normalize(GetVector());
        }
        else if(operation == "quit"){
            break;
        }
        else{
            cout << "Wrong operation!" << endl;
            continue;
        }

        if(operation == "length"){
            cout << "Vector length = " << res << "." << endl;
        }
        else{
            cout << "Result vector coordinates: x = " << resVector.x << ", y = " << resVector.y << "." << endl;
        }

    }
    return 0;
}
