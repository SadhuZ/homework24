#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

struct personalData
{
    string name;
    string surename;
    string date;
    float payment;
};

bool PersonalDataRead(vector<personalData>& persons, const char* fileName){
    ifstream vedomost;
    vedomost.open(fileName);
    if(!vedomost.is_open()){
        cout << "File " << fileName << " open error." << endl;
        return false;
    }
    while(!vedomost.eof()){
        personalData person;
        vedomost >> person.name >> person.surename >> person.date >> person.payment;
        if(person.name.size() > 0)
            persons.push_back(person);
    }
    vedomost.close();
    return true;
}

void PersonalDataWrite(personalData& person, const char* fileName){
    ofstream vedomost;
    vedomost.open(fileName, ios::app);
    vedomost.setf(ios::showpoint);
    vedomost.setf(ios::fixed);
    vedomost.precision(2);
    vedomost << person.name << " " << person.surename << " " << person.date << " " << person.payment << endl;
    vedomost.close();
}

bool IsDate(string date){
    if(stoi(date.substr(0, 2)) >= 1 && stoi(date.substr(0, 2)) <= 31 &&
            stoi(date.substr(3, 2)) >= 1 && stoi(date.substr(3, 2)) <= 12 &&
            stoi(date.substr(7, 4)) >= 1 && stoi(date.substr(7, 4)) <= 9999 &&
            date[2] == '.' && date[5] == '.' && date.size() == 10)
        return true;
    return false;
}

int main()
{
    const char* fileName = "vedomost.txt";
    string cmd;
    cout << "Enter command: ";
    cin >> cmd;

    if(cmd == "write"){
        personalData person;
        cout << "Name: ";
        cin >> person.name;
        cout << "Surname: ";
        cin >> person.surename;
        while(1){
            cout << "Date (dd.mm.yyyy): ";
            cin >> person.date;
            if(IsDate(person.date))
                break;
            cout << "Wrong date!" << endl;
        }
        cout << "Payment: ";
        cin >> person.payment;
        PersonalDataWrite(person, fileName);
    }
    else if(cmd == "read"){
        vector<personalData> person;
        if(PersonalDataRead(person, fileName)){
            for(size_t i = 0; i < person.size(); i++){
                cout << person[i].name << " " << person[i].surename << " " << person[i].date << " " << person[i].payment << endl;
            }
        }
    }
    return 0;
}
