#include <iostream>
#include <vector>
#include <fstream>
#include <time.h>

using namespace std;

const int fieldSize = 40;

struct person
{
    string name;
    int life;
    int armor;
    int damage;
    int x;
    int y;
};

void SavePerson(ofstream& file, const person& pers){
    int length = pers.name.length();
    file.write((char*)&length, sizeof (int));
    file.write(pers.name.c_str(), pers.name.length());
    file.write((char*)&pers.life, sizeof (int));
    file.write((char*)&pers.armor, sizeof (int));
    file.write((char*)&pers.damage, sizeof (int));
    file.write((char*)&pers.x, sizeof (int));
    file.write((char*)&pers.y, sizeof (int));
}

void SaveBattleField(const person& hero, const vector<person>& enemies){
    ofstream file;
    file.open("BattleField.save", ios::binary);
    if(!file.is_open()){
         cout << "File open error." << endl;
         return;
    }
    SavePerson(file, hero);
    for(size_t i = 0; i < enemies.size(); ++i)
        SavePerson(file, enemies[i]);
    file.close();
}

void LoadPerson(ifstream& file, person& pers){
    int length;
    file.read((char*)&length, sizeof (int));
    pers.name.resize(length);
    file.read((char*)pers.name.c_str(), length);
    file.read((char*)&pers.life, sizeof (int));
    file.read((char*)&pers.armor, sizeof (int));
    file.read((char*)&pers.damage, sizeof (int));
    file.read((char*)&pers.x, sizeof (int));
    file.read((char*)&pers.y, sizeof (int));
}

void LoadBattleField(person& hero, vector<person>& enemies){
    ifstream file;
    file.open("BattleField.save", ios::binary);
    if(!file.is_open()){
         cout << "File open error." << endl;
         return;
    }
    LoadPerson(file, hero);
    for(size_t i = 0; i < enemies.size(); ++i)
        LoadPerson(file, enemies[i]);
    file.close();
}

person CreateHero()
{
    person hero;
    cout << "Enter hero name: ";
    cin >> hero.name;
    cout << "Enter hero life: ";
    cin >> hero.life;
    cout << "Enter hero armor: ";
    cin >> hero.armor;
    cout << "Enter hero damage: ";
    cin >> hero.damage;
    hero.x = 0;
    hero.y = 0;
    return hero;
}

void TakeDamage(person& hero, person& enemy){
    hero.armor = hero.armor - enemy.damage;
    if(hero.armor < 0){
        hero.life += hero.armor;
        hero.armor = 0;
    }
    enemy.armor = enemy.armor - hero.damage;
    if(enemy.armor < 0){
        enemy.life += enemy.armor;
        enemy.armor = 0;
    }
}

bool HeroStopCollision(person& hero, vector<person>& enemies){
    for(size_t i = 0; i < enemies.size(); ++i){
        if(enemies[i].x == hero.x && enemies[i].y == hero.y){
            TakeDamage(hero, enemies[i]);
            if(enemies[i].life <= 0){
                enemies.erase(enemies.begin() + i);
                return false;
            }
            return true;
        }
    }
    return false;
}



void HeroMove(person& hero, vector<person>& enemies){
    string command;
    cout << "Move the hero (l, r, t, b, save, load): ";
    cin >> command;
    if(command == "save"){
        SaveBattleField(hero, enemies);
    }
    else if (command == "load"){
        LoadBattleField(hero, enemies);
    }
    else{
        int saveX = hero.x;
        int saveY = hero.y;
        if (command == "l"){
            if(hero.x > 0)
                hero.x--;
        }
        else if (command == "r"){
            if(hero.x < fieldSize - 1)
                hero.x++;
        }
        else if (command == "t"){
            if(hero.y > 0)
                hero.y--;
        }
        else if (command == "b"){
            if(hero.y < fieldSize - 1)
                hero.y++;
        }
        if(HeroStopCollision(hero, enemies)){
            hero.x = saveX;
            hero.y = saveY;
        }
    }
}

person CreateEnemy(const vector<person>& enemies)
{
    person enemy;
    enemy.name = "Enemy " + to_string(enemies.size() + 1);
    enemy.life = 50 + rand() % 101;
    enemy.armor = rand() % 51;
    enemy.damage = 15 + rand() % 16;

    bool isBadCoordinates = true;
    while(isBadCoordinates){
        enemy.x = rand() % 40;
        enemy.y = rand() % 40;
        for(size_t i = 0; i < enemies.size(); ++i){
            if(enemies[i].x == enemy.x && enemies[i].y == enemy.y)
                break;
        }
        isBadCoordinates = false;
    }

    return enemy;
}

void EnemiesMove(person& hero, vector<person>& enemies){
    string command;
    int stepX;
    int stepY;
    bool collision;
    for(size_t i = 0; i < enemies.size(); ++i){
        stepX = 1 - rand() % 3;
        stepY = 0;
        if(stepX == 0)
            stepY = 1 - rand() % 3;
        collision = false;
        for(size_t j = 0; j < enemies.size(); ++j){
            if(i != j){
                if(enemies[i].x == enemies[j].x && enemies[i].y == enemies[j].y){
                    collision = true;
                    break;
                }
            }
        }
        if(!collision){
            if((enemies[i].x  + stepX) == hero.x && (enemies[i].y  + stepY) == hero.y){
                TakeDamage(hero, enemies[i]);
                if(enemies[i].life <= 0)
                    enemies.erase(enemies.begin() + i);
                else
                    collision = true;
            }
        }
        if(!collision){
            int coordinate = enemies[i].x + stepX;
            if(coordinate >= 0 && coordinate < fieldSize)
                enemies[i].x = coordinate;
            coordinate = enemies[i].y + stepY;
            if(coordinate >= 0 && coordinate < fieldSize)
                enemies[i].y = coordinate;
        }
    }
}

void DrawBattleField(const person& hero, const vector<person>& enemies){
    string battleField[fieldSize];
    for(int y = 0; y < fieldSize; ++y){
        for(int x = 0; x < fieldSize; ++x){
            battleField[y].append(".");
        }
    }

    for(size_t i = 0; i < enemies.size(); ++i){
        battleField[enemies[i].y][enemies[i].x] = 'E';
    }
    battleField[hero.y][hero.x] = 'H';

    for(int y = 0; y < fieldSize; ++y){
        cout << battleField[y] << endl;
    }
}

void PrintPersonParameters(const person& pers){
    cout << "Name: " << pers.name << ", "
        << "Life: " << pers.life << ", "
        << "Armor: " << pers.armor << ", "
        << "Damage: " << pers.damage << "." << endl;
}

bool IsGameOver(const person& hero, const vector<person>& enemies){
    if(hero.life <= 0){
        cout << "You lose!" << endl;
        return true;
    }
    else if(enemies.size() == 0){
        cout << "You win!" << endl;
        return true;
    }
    return false;
}

int main()
{    
    srand(time(NULL));
    person hero = CreateHero();
    vector<person> enemies;
    for(int i = 0; i < 5; ++i)
        enemies.push_back(CreateEnemy(enemies));
    DrawBattleField(hero, enemies);

    while(1){
        HeroMove(hero, enemies);
        if(IsGameOver(hero, enemies))
            break;
        EnemiesMove(hero, enemies);
        DrawBattleField(hero, enemies);
        if(IsGameOver(hero, enemies))
            break;

        PrintPersonParameters(hero);
        for(size_t i = 0; i < enemies.size(); ++i)
            PrintPersonParameters(enemies[i]);
    }

    return 0;
}
