#include <iostream>
#include <vector>

using namespace std;

enum RoomType{
    BEDROOM,
    KITCHEN,
    BATHROOM,
    PLAYROOM,
    LIVINGROOM
};

struct room
{
    int square;
    RoomType type;
};

void SetRoom(room* oneRoom)
{
    cout << "Введите тип комнаты ";
    cout << "(" << BEDROOM << " - спальня, " << KITCHEN << " - кухня, " << BATHROOM << " - ванная, "
        << PLAYROOM << " - детская, " << LIVINGROOM << " - гостинная): ";
    int type;
    cin >> type;
    oneRoom->type = (RoomType)type;
    cout << "Введите площадь комнаты: ";
    cin >> oneRoom->square;
}

struct floor
{
    int height;
    vector<room> rooms;
};

void SetFloor(floor* oneFloor)
{
    int roomsNum;
    cout << "Введите высоту потолков: ";
    cin >> oneFloor->height;
    cout << "Введите количество комнат: ";
    cin >> roomsNum;
    for(int r = 0; r < roomsNum; r++){
        cout << "Комната " << r + 1 << ".";;
        room oneRoom;
        SetRoom(&oneRoom);
        oneFloor->rooms.push_back(oneRoom);
    }
}

enum BuildingType{
    HOUSE,
    GARAGE,
    BARN,
    BATH
};

struct house
{
    bool stove;
    vector<floor> floors;
};
void SetHouse(house* oneHouse)
{
    int floorsNum;
    cout << "Введите наличие печи с трубой (1 - да, 0 - нет): ";
    cin >> oneHouse->stove;
    cout << "Введите количество этажей: ";
    cin >> floorsNum;
    for(int f = 0; f < floorsNum; f++){
        cout << "Этаж " << f + 1 << ".";;
        floor oneFloor;
        SetFloor(&oneFloor);
        oneHouse->floors.push_back(oneFloor);
    }
}
int GetHouseSquare(const house& oneHouse){
    int square = 0;
    for(size_t f = 0; f < oneHouse.floors.size(); f++){
        for(size_t r = 0; r < oneHouse.floors[f].rooms.size(); r++){
            square += oneHouse.floors[f].rooms[r].square;
        }
    }
    return square;
}

struct garage
{
    int square;
};

void SetGarage(garage* oneGarage)
{
    cout << "Введите площадь гаража: ";
    cin >> oneGarage->square;
}

struct barn
{
    int square;
};

void SetBarn(barn* oneBarn)
{
    cout << "Введите площадь сарая: ";
    cin >> oneBarn->square;
}

struct bath
{
    int square;
    bool stove;
};

void SetBath(bath* oneBath)
{
    cout << "Введите площадь бани: ";
    cin >> oneBath->square;
    cout << "Введите наличие печи с трубой (1 - да, 0 - нет): ";
    cin >> oneBath->stove;
}


struct plot
{
    vector<house> houses;
    vector<garage> garages;
    vector<barn> barns;
    vector<bath> baths;
    int square;
};

void SetPlot(plot* onePlot)
{
    int buildingsNum;
    cout << "Введите площадь участка: ";
    cin >> onePlot->square;
    cout << "Введите количество построек на участке: ";
    cin >> buildingsNum;
    for(int building = 0; building < buildingsNum; building++){
        int buildingType;
        house oneHouse;
        garage oneGarage;
        barn oneBarn;
        bath oneBath;
        cout << "Введите тип " << building + 1 << "-й постройки ";
        cout << "(" << HOUSE << " - дом, " << GARAGE << " - гараж, " << BARN << " - сарай, " << BATH << " - баня): ";
        cin >> buildingType;
        switch (buildingType) {
            case HOUSE:
                SetHouse(&oneHouse);
                onePlot->houses.push_back(oneHouse);
            break;
            case GARAGE:
                SetGarage(&oneGarage);
                onePlot->garages.push_back(oneGarage);
            break;
            case BARN:
                SetBarn(&oneBarn);
                onePlot->barns.push_back(oneBarn);
            break;
            case BATH:
                SetBath(&oneBath);
                onePlot->baths.push_back(oneBath);
            break;
        }
    }
}

int main()
{
    vector<plot> plots;
    int plotsNum;
    cout << "Введите количество участков: ";
    cin >> plotsNum;
    plots.resize(plotsNum);
    for(size_t i = 0; i < plots.size(); i++){
        cout << "Участок " << i + 1 << "." << endl;
        SetPlot(&plots[i]);
    }

    for(size_t i = 0; i < plots.size(); i++){
        cout << "Участок " << i + 1 << "." << endl;
        for(size_t h = 0; h < plots[i].houses.size(); h++){
            cout << "Площадь дома " << h + 1 << ": " << GetHouseSquare(plots[i].houses.at(h)) << "." << endl;
        }
    }
    return 0;
}
